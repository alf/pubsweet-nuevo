const { isEmail } = require('validator')

module.exports = mongoose => {
  const schema = mongoose.Schema({
    username: {
      type: String,
      required: true,
      unique: true
    },
    email: {
      type: String,
      required: true,
      unique: true,
      validate: [isEmail, 'invalid email']
    },
    name: {
      type: String,
      required: true
    },
    password: {
      type: String,
      required: true
    }
  }, {
    timestamps: true,
    toJSON: {
      transform: (doc, ret, options) => {
        delete ret.password
        return ret
      }
    }
  })

  return mongoose.model('User', schema)
}
