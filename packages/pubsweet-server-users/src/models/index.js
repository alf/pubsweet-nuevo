const User = require('./User')

module.exports = app => {
  app.locals.models.User = User(app.locals.mongoose)
}
