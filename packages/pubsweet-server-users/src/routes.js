module.exports = app => {
  const authentication = app.locals.authentication
  const authorize = app.locals.authorize
  const encryption = app.locals.encryption
  const models = app.locals.models

  const preSave = async (req, data) => {
    // TODO: use roles instead of User.admin?

    if (data['admin'] !== undefined && !req.user.admin) {
      throw new Error('Setting admin is forbidden')
    }

    if (data['password']) {
      data['password'] = await encryption.hash(data['password'])
    }
  }

  // load the item
  app.param('user', async (req, res, next, id) => {
    req.item = await models.User.findById(id)

    if (!req.item) {
      req.sendStatus(404)
      return next(new Error('Not Found'))
    }

    next()
  })

  app.route('/users')
    // middleware for all methods
    .all(
      authentication.bearer,
      authorize('users')
    ) // TODO: will this run on "/users/:user"?

    // list users
    .get(async (req, res, next) => {
      try {
        const items = await models.User.find() // TODO: query filter

        return res.json(items)
      } catch (e) {
        next(e)
      }
    })

    // create a user
    .post(async (req, res, next) => {
      try {
        const data = req.body
        await preSave(req, data)

        const item = new models.User(data)
        await item.save()

        return res.status(201).json(item)
      } catch (e) {
        next(e)
      }
    })

  app.route('/users/:user')
    // middleware for all methods
    .all(
      authentication.bearer,
      authorize('user')
    )

    // retrieve a user
    .get(async (req, res, next) => {
      try {
        const item = req.item

        res.json(item)
      } catch (e) {
        next(e)
      }
    })

    // update a user
    .patch(async (req, res, next) => {
      try {
        const data = req.body
        await preSave(req, data)

        const item = req.item
        await item.update(data)

        res.json(item)
      } catch (e) {
        next(e)
      }
    })

    // delete a user
    .delete(async (req, res, next) => {
      try {
        const item = req.item

        await item.remove()

        res.sendStatus(204)
      } catch (e) {
        next(e)
      }
    })
}
