// needs to be isomorphic-fetch so works in Jest tests (Node)
import fetch from 'isomorphic-fetch'
import { token as authToken } from '@aeaton/pubsweet-client-authentication'

const parse = response => {
  if (response.headers.get('content-type').indexOf('application/json') !== -1) {
    return response.json()
  }

  return response
}

const request = function (path, options = {}) {
  options.headers = options.headers || {}
  options.headers['Accept'] = options.headers['Accept'] || 'application/json'

  // read the authentication token from localStorage
  const token = authToken.get()

  if (token) {
    options.headers['Authorization'] = 'Bearer ' + token
  }

  // build the full URL
  const url = process.env.REACT_APP_PUBSWEET_API + path

  return fetch(url, options).then(response => {
    if (!response.ok) {
      const error = new Error(response.statusText || response.status)
      error.message = response.text() // NOTE: this is a promise
      throw error
    }

    return parse(response)
  })
  // .catch(error => {
  //   // TODO: handle network errors
  //   console.error(error)
  // })
}

export const get = (url) => request(url, {
  method: 'GET'
})

export const create = (url, data) => request(url, {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(data)
})

export const update = (url, data, replace = false) => request(url, {
  method: replace ? 'PUT' : 'PATCH',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify(data)
})

export const remove = (url, data) => request(url, {
  method: 'DELETE'
})

export default request

// TODO: export an ApiClient class instead?
