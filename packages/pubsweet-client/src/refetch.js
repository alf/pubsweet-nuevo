import 'isomorphic-fetch' // for global.Request
import { connect } from 'react-refetch'
import { token as authToken } from '@aeaton/pubsweet-client-authentication'

export default connect.defaults({
  buildRequest: function (options) {
    const token = authToken.get()

    if (token) {
      options.headers['Authorization'] = 'Bearer ' + token
    }

    options.url = process.env.REACT_APP_PUBSWEET_API + options.url

    return new global.Request(options.url, options)
  }
})
