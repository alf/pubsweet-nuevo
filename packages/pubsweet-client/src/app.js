import React from 'react'
import ReactDOM from 'react-dom'
import { Provider as StoreProvider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import { applyMiddleware, combineReducers, createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk from 'redux-thunk'
import { reducers } from '@aeaton/pubsweet-client-authentication'

export default ({ App }) => {
  const store = createStore(
    combineReducers(reducers),
    composeWithDevTools(applyMiddleware(thunk))
  )

  const root = (
    <StoreProvider store={store}>
      <BrowserRouter>
        <App/>
      </BrowserRouter>
    </StoreProvider>
  )

  ReactDOM.render(root, document.getElementById('root'))
}
