'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

require('isomorphic-fetch');

var _reactRefetch = require('react-refetch');

var _pubsweetClientAuthentication = require('@aeaton/pubsweet-client-authentication');

// for global.Request
exports.default = _reactRefetch.connect.defaults({
  buildRequest: function buildRequest(options) {
    var token = _pubsweetClientAuthentication.token.get();

    if (token) {
      options.headers['Authorization'] = 'Bearer ' + token;
    }

    options.url = process.env.REACT_APP_PUBSWEET_API + options.url;

    return new global.Request(options.url, options);
  }
});