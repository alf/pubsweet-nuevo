'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.remove = exports.update = exports.create = exports.get = undefined;

var _isomorphicFetch = require('isomorphic-fetch');

var _isomorphicFetch2 = _interopRequireDefault(_isomorphicFetch);

var _pubsweetClientAuthentication = require('@aeaton/pubsweet-client-authentication');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// needs to be isomorphic-fetch so works in Jest tests (Node)
var parse = function parse(response) {
  if (response.headers.get('content-type').indexOf('application/json') !== -1) {
    return response.json();
  }

  return response;
};

var request = function request(path) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  options.headers = options.headers || {};
  options.headers['Accept'] = options.headers['Accept'] || 'application/json';

  // read the authentication token from localStorage
  var token = _pubsweetClientAuthentication.token.get();

  if (token) {
    options.headers['Authorization'] = 'Bearer ' + token;
  }

  // build the full URL
  var url = process.env.REACT_APP_PUBSWEET_API + path;

  return (0, _isomorphicFetch2.default)(url, options).then(function (response) {
    if (!response.ok) {
      var error = new Error(response.statusText || response.status);
      error.message = response.text(); // NOTE: this is a promise
      throw error;
    }

    return parse(response);
  });
  // .catch(error => {
  //   // TODO: handle network errors
  //   console.error(error)
  // })
};

var get = exports.get = function get(url) {
  return request(url, {
    method: 'GET'
  });
};

var create = exports.create = function create(url, data) {
  return request(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
};

var update = exports.update = function update(url, data) {
  var replace = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
  return request(url, {
    method: replace ? 'PUT' : 'PATCH',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  });
};

var remove = exports.remove = function remove(url, data) {
  return request(url, {
    method: 'DELETE'
  });
};

exports.default = request;

// TODO: export an ApiClient class instead?