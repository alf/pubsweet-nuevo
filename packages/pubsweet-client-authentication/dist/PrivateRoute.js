'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouterDom = require('react-router-dom');

var _actions = require('./redux/actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

var PrivateRoute = function PrivateRoute(_ref) {
  var user = _ref.user,
      fetchUser = _ref.fetchUser,
      component = _ref.component,
      rest = _objectWithoutProperties(_ref, ['user', 'fetchUser', 'component']);

  return _react2.default.createElement(_reactRouterDom.Route, _extends({}, rest, { render: function render(props) {
      if (!user.complete) {
        if (!user.fetching) {
          fetchUser();
        }

        return null; // TODO: loading screen
      }

      // TODO: handle user.error

      if (!user.data) {
        return _react2.default.createElement(_reactRouterDom.Redirect, { to: {
            pathname: '/login',
            state: {
              from: rest.location
            }
          } });
      }

      return _react2.default.createElement(component, props);
    } }));
};

exports.default = (0, _reactRouterDom.withRouter)((0, _reactRedux.connect)(function (state) {
  return {
    user: state.user
  };
}, {
  fetchUser: _actions.fetchUser
})(PrivateRoute));