'use strict';

var localStorage = window.localStorage || global.window.localStorage;

module.exports = {
  set: function set(token) {
    return localStorage.setItem('token', token);
  },
  get: function get() {
    return localStorage.getItem('token');
  },
  remove: function remove() {
    return localStorage.removeItem('token');
  }
};