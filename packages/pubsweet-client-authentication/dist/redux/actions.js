'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.logout = exports.fetchUser = exports.login = exports.signup = undefined;

var _pubsweetClient = require('@aeaton/pubsweet-client');

var _constants = require('./constants');

var _token = require('../token');

var _token2 = _interopRequireDefault(_token);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var signupRequest = function signupRequest() {
  return {
    type: _constants.SIGNUP_REQUEST
  };
};

var signupSuccess = function signupSuccess() {
  return {
    type: _constants.SIGNUP_SUCCESS
  };
};

var signupFailure = function signupFailure(error) {
  return {
    type: _constants.SIGNUP_FAILURE,
    error: error
  };
};

var loginRequest = function loginRequest(credentials) {
  return {
    type: _constants.LOGIN_REQUEST,
    credentials: credentials
  };
};

var loginSuccess = function loginSuccess() {
  return {
    type: _constants.LOGIN_SUCCESS
  };
};

var loginFailure = function loginFailure(error) {
  return {
    type: _constants.LOGIN_FAILURE,
    error: error
  };
};

var userRequest = function userRequest() {
  return {
    type: _constants.USER_REQUEST
  };
};

var userSuccess = function userSuccess(user) {
  return {
    type: _constants.USER_SUCCESS,
    user: user
  };
};

var userFailure = function userFailure(error) {
  return {
    type: _constants.USER_FAILURE,
    error: error
  };
};

var logoutSuccess = function logoutSuccess() {
  return {
    type: _constants.LOGOUT
  };
};

var signup = exports.signup = function signup(data) {
  return function (dispatch) {
    dispatch(signupRequest());

    return _pubsweetClient.api.create('/authentication/signup', data).catch(function (err) {
      console.error(err);
      dispatch(signupFailure(err.message));
    }).then(function (response) {
      _token2.default.set(response.token);
      dispatch(signupSuccess());
      dispatch(userSuccess(response.user));
    });
  };
};

var login = exports.login = function login(credentials) {
  return function (dispatch) {
    dispatch(loginRequest(credentials));

    return _pubsweetClient.api.create('/authentication/login', credentials).then(function (response) {
      _token2.default.set(response.token);
      dispatch(loginSuccess());
      dispatch(userSuccess(response.user));
    }).catch(function (err) {
      console.error(err);
      dispatch(loginFailure(err.message));
    });
  };
};

var fetchUser = exports.fetchUser = function fetchUser() {
  return function (dispatch) {
    dispatch(userRequest());

    return _pubsweetClient.api.get('/authentication/user').then(function (response) {
      dispatch(userSuccess(response.user));
    }).catch(function (err) {
      console.error(err);
      dispatch(userFailure(err.message));
    });
  };
};

var logout = exports.logout = function logout() {
  return function (dispatch) {
    _token2.default.remove();
    dispatch(logoutSuccess());
  };
};