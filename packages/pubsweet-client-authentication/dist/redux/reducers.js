'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.user = exports.login = exports.signup = undefined;

var _constants = require('./constants');

var signup = exports.signup = function signup() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    fetching: false,
    complete: false,
    error: null
  };
  var action = arguments[1];

  switch (action.type) {
    case _constants.SIGNUP_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null
      });

    case _constants.SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null
      });

    case _constants.SIGNUP_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: false,
        error: action.error
      });

    default:
      return state;
  }
};

var login = exports.login = function login() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    fetching: false,
    complete: false,
    error: null
  };
  var action = arguments[1];

  switch (action.type) {
    case _constants.LOGIN_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null
      });

    case _constants.LOGIN_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null
      });

    case _constants.LOGIN_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: action.error
      });

    default:
      return state;
  }
};

var user = exports.user = function user() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    fetching: false,
    complete: false,
    error: null,
    data: null
  };
  var action = arguments[1];

  switch (action.type) {
    case _constants.USER_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null,
        data: null
      });

    case _constants.USER_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null,
        data: action.user
      });

    case _constants.USER_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: action.error,
        data: null
      });

    case _constants.LOGOUT:
      return Object.assign({}, state, {
        data: null
      });

    default:
      return state;
  }
};