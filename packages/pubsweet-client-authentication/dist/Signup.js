'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _reactRouterDom = require('react-router-dom');

var _reactBootstrap = require('react-bootstrap');

var _formsyReactComponents = require('formsy-react-components');

var _formsyReactComponents2 = _interopRequireDefault(_formsyReactComponents);

var _actions = require('./redux/actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Signup = function Signup(_ref) {
  var error = _ref.error,
      signup = _ref.signup;
  return _react2.default.createElement(
    'div',
    { className: 'container' },
    _react2.default.createElement(
      _reactBootstrap.Row,
      null,
      _react2.default.createElement(
        _reactBootstrap.Col,
        { md: 4 },
        error && _react2.default.createElement(
          _reactBootstrap.Alert,
          { bsStyle: 'warning' },
          _react2.default.createElement('i', { className: 'fa fa-exclamation-circle' }),
          _react2.default.createElement(
            'span',
            null,
            error
          )
        )
      ),
      _react2.default.createElement(
        _reactBootstrap.Col,
        { xs: 12, md: 4 },
        _react2.default.createElement(
          'h1',
          null,
          'Create an account'
        ),
        _react2.default.createElement(
          _formsyReactComponents2.default.Form,
          { onSubmit: signup, validateOnSubmit: true, layout: 'vertical' },
          _react2.default.createElement(_formsyReactComponents2.default.Input, { type: 'text', name: 'name', label: 'Name', required: true }),
          _react2.default.createElement(_formsyReactComponents2.default.Input, { type: 'text', name: 'username', label: 'Username', required: true }),
          _react2.default.createElement(_formsyReactComponents2.default.Input, { type: 'email', name: 'email', label: 'Email', required: true }),
          _react2.default.createElement(_formsyReactComponents2.default.Input, { type: 'password', name: 'password', label: 'Password', required: true }),
          _react2.default.createElement(
            _reactBootstrap.Button,
            { type: 'submit', bsStyle: 'primary', bsSize: 'large' },
            'Register'
          ),
          _react2.default.createElement(
            'span',
            null,
            ' or '
          ),
          _react2.default.createElement(
            _reactRouterDom.Link,
            { to: '/login' },
            'sign in'
          )
        )
      )
    )
  );
};

exports.default = (0, _reactRedux.connect)(function (state) {
  return {
    error: state.signup.error
  };
}, {
  signup: _actions.signup
})(Signup);