import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Row, Col, Alert, Button } from 'react-bootstrap'
import FRC from 'formsy-react-components'
import { signup } from './redux/actions'

const Signup = ({ error, signup }) => (
  <div className="container">
    <Row>
      <Col md={4}>
        {error && (
          <Alert bsStyle="warning">
            <i className="fa fa-exclamation-circle"/>
            <span>{error}</span>
          </Alert>
        )}
      </Col>

      <Col xs={12} md={4}>
        <h1>Create an account</h1>

        <FRC.Form onSubmit={signup} validateOnSubmit={true} layout="vertical">
          <FRC.Input type="text" name="name" label="Name" required/>
          <FRC.Input type="text" name="username" label="Username" required/>
          <FRC.Input type="email" name="email" label="Email" required/>
          <FRC.Input type="password" name="password" label="Password" required/>
          <Button type="submit" bsStyle="primary" bsSize="large">Register</Button>
          <span> or </span>
          <Link to="/login">sign in</Link>
        </FRC.Form>
      </Col>
    </Row>
  </div>
)

export default connect(
  state => ({
    error: state.signup.error
  }),
  {
    signup
  }
)(Signup)
