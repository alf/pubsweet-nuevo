import React from 'react'
import { connect } from 'react-redux'
import { Redirect, Route, withRouter } from 'react-router-dom'
import { fetchUser } from './redux/actions'

const PrivateRoute = ({ user, fetchUser, component, ...rest }) => (
  <Route {...rest} render={props => {
    if (!user.complete) {
      if (!user.fetching) {
        fetchUser()
      }

      return null // TODO: loading screen
    }

    // TODO: handle user.error

    if (!user.data) {
      return <Redirect to={{
        pathname: '/login',
        state: {
          from: rest.location
        }
      }}/>
    }

    return React.createElement(component, props)
  }}/>
)

export default withRouter(connect(
  state => ({
    user: state.user
  }),
  {
    fetchUser
  }
)(PrivateRoute))
