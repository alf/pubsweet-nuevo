import React from 'react'
import { connect } from 'react-redux'
import { Link, Redirect } from 'react-router-dom'
import { Row, Col, Alert, Button } from 'react-bootstrap'
import FRC from 'formsy-react-components'
import { login } from './redux/actions'

const Login = ({ error, login, user }) => {
  if (user) return <Redirect to="/"/>

  return (
    <div className="container">
      <Row>
        <Col md={4}>
          {error && (
            <Alert bsStyle="warning">
              <i className="fa fa-exclamation-circle"/>
              <span>{error}</span>
            </Alert>
          )}
        </Col>

        <Col xs={12} md={4}>
          <h1>Sign in</h1>

          <FRC.Form onSubmit={login} validateOnSubmit={true} layout="vertical">
            <FRC.Input type="text" name="username" label="Username" required/>
            <FRC.Input type="password" name="password" label="Password" required/>
            <Button type="submit" bsStyle="primary" bsSize="large">Sign In</Button>
            <span> or </span>
            <Link to="/signup">sign up</Link>
            <span> / </span>
            <Link to="/password-reset">reset password</Link>
          </FRC.Form>
        </Col>
      </Row>
    </div>
  )
}

export default connect(
  state => ({
    user: state.user.data,
    error: state.login.error
  }),
  {
    login
  }
)(Login)
