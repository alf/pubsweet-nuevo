const localStorage = window.localStorage || global.window.localStorage

module.exports = {
  set: token => localStorage.setItem('token', token),
  get: () => localStorage.getItem('token'),
  remove: () => localStorage.removeItem('token')
}
