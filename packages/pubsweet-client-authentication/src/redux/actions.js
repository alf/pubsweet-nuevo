import { api } from '@aeaton/pubsweet-client'

import { SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT, USER_REQUEST, USER_SUCCESS, USER_FAILURE } from './constants'

import authToken from '../token'

const signupRequest = () => ({
  type: SIGNUP_REQUEST
})

const signupSuccess = () => ({
  type: SIGNUP_SUCCESS
})

const signupFailure = error => ({
  type: SIGNUP_FAILURE,
  error
})

const loginRequest = credentials => ({
  type: LOGIN_REQUEST,
  credentials
})

const loginSuccess = () => ({
  type: LOGIN_SUCCESS
})

const loginFailure = error => ({
  type: LOGIN_FAILURE,
  error
})

const userRequest = () => ({
  type: USER_REQUEST
})

const userSuccess = user => ({
  type: USER_SUCCESS,
  user
})

const userFailure = error => ({
  type: USER_FAILURE,
  error
})

const logoutSuccess = () => ({
  type: LOGOUT
})

export const signup = data => dispatch => {
  dispatch(signupRequest())

  return api.create('/authentication/signup', data).catch(err => {
    console.error(err)
    dispatch(signupFailure(err.message))
  }).then(response => {
    authToken.set(response.token)
    dispatch(signupSuccess())
    dispatch(userSuccess(response.user))
  })
}

export const login = credentials => dispatch => {
  dispatch(loginRequest(credentials))

  return api.create('/authentication/login', credentials).then(response => {
    authToken.set(response.token)
    dispatch(loginSuccess())
    dispatch(userSuccess(response.user))
  }).catch(err => {
    console.error(err)
    dispatch(loginFailure(err.message))
  })
}

export const fetchUser = () => dispatch => {
  dispatch(userRequest())

  return api.get('/authentication/user').then(response => {
    dispatch(userSuccess(response.user))
  }).catch(err => {
    console.error(err)
    dispatch(userFailure(err.message))
  })
}

export const logout = () => dispatch => {
  authToken.remove()
  dispatch(logoutSuccess())
}
