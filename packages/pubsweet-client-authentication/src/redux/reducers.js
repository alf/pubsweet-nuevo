import { SIGNUP_REQUEST, SIGNUP_SUCCESS, SIGNUP_FAILURE, LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT, USER_REQUEST, USER_SUCCESS, USER_FAILURE } from './constants'

export const signup = (state = {
  fetching: false,
  complete: false,
  error: null
}, action) => {
  switch (action.type) {
    case SIGNUP_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null
      })

    case SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null
      })

    case SIGNUP_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: false,
        error: action.error
      })

    default:
      return state
  }
}

export const login = (state = {
  fetching: false,
  complete: false,
  error: null
}, action) => {
  switch (action.type) {
    case LOGIN_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null
      })

    case LOGIN_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null
      })

    case LOGIN_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: action.error
      })

    default:
      return state
  }
}

export const user = (state = {
  fetching: false,
  complete: false,
  error: null,
  data: null
}, action) => {
  switch (action.type) {
    case USER_REQUEST:
      return Object.assign({}, state, {
        fetching: true,
        complete: false,
        error: null,
        data: null
      })

    case USER_SUCCESS:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: null,
        data: action.user
      })

    case USER_FAILURE:
      return Object.assign({}, state, {
        fetching: false,
        complete: true,
        error: action.error,
        data: null
      })

    case LOGOUT:
      return Object.assign({}, state, {
        data: null
      })

    default:
      return state
  }
}
