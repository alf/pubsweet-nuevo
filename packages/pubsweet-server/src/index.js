const app = require('./app')
const logger = require('./logger')

module.exports = { app, logger }

process.on('unhandledRejection', err => {
  throw err
})
