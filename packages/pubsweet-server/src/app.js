const authorization = require('@aeaton/pubsweet-server-authorization')
const bodyParser = require('body-parser')
const cors = require('cors')
const compression = require('compression')
const errorhandler = require('errorhandler')
const express = require('express')
const helmet = require('helmet')
const mongoose = require('mongoose')
const morgan = require('morgan')
const logger = require('./logger') // TODO: pubsweet-server-logger?

module.exports = options => {
  const app = express()

  // utilities
  app.use(bodyParser.json())
  app.use(cors())
  app.use(helmet())
  app.use(compression())
  app.use(morgan('tiny', { stream: logger.stream }))

  // locals
  app.locals.authorize = authorization(options.rules)
  app.locals.components = options.components
  app.locals.logger = logger
  app.locals.models = {}
  app.locals.mongoose = mongoose

  // PubSweet components
  options.components.forEach(component => component(app))

  // development-only error handler
  if (process.env.NODE_ENV === 'development') {
    app.use(errorhandler())
  }

  // connect to the database and start the server
  mongoose.Promise = global.Promise
  mongoose.connect(process.env.PUBSWEET_MONGODB).then(() => {
    const port = parseInt(process.env.PORT, 10) || 4000
    const host = process.env.HOST || '0.0.0.0'
    const server = app.listen(port, host)

    server.on('listening', () => {
      const { address, port } = server.address()
      const protocol = process.env.HTTPS === 'true' ? 'https' : 'http'
      const url = `${protocol}://${address}:${port}`
      console.log('Listening at', url)
    })
  })

  return app
}
