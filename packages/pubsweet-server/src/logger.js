const path = require('path')
const winston = require('winston')
winston.emitErrs = true

const logger = new winston.Logger({
  transports: [
    new winston.transports.File({
      level: 'info',
      filename: path.join(__dirname, '..', '..', '..', 'logs', 'combined.log'),
      handleExceptions: true,
      json: true,
      maxFiles: 5,
      colorize: false
    }),
    new winston.transports.Console({
      level: 'debug',
      handleExceptions: true,
      json: false,
      colorize: true
    })
  ],
  exitOnError: false
})

module.exports = logger

module.exports.stream = {
  write: function (message, encoding) {
    logger.info(message)
  }
}
