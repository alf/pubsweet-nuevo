module.exports = rules => section => {
  if (!rules[section]) {
    throw new Error('Missing rules section: ' + section)
  }

  return (req, res, next) => {
    const method = req.method.toLowerCase()

    console.log(section, method, req.originalUrl, rules)

    const rule = rules[section][method]

    if (!rule) {
      return next(new Error('Unauthorized (no rule)'))
    }

    if (!rule(req.user, req.item, req.body)) {
      return next(new Error('Unauthorized'))
    }

    next()
  }
}
