module.exports = app => {
  const authentication = app.locals.authentication
  const authorize = app.locals.authorize
  const models = app.locals.models

  const preSave = async (req, data) => {
    data.owner = String(req.user._id) // TODO: use roles?
  }

  // load the item
  app.param('project', async (req, res, next, id) => {
    req.item = await models.Project.findById(id)

    if (!req.item) {
      req.sendStatus(404)
      return next(new Error('Not Found'))
    }

    next()
  })

  app.route('/projects')
    // middleware for all methods
    .all(
      authentication.bearer,
      authorize('projects')
    )

    // list projects
    .get(async (req, res, next) => {
      try {
        const items = await models.Project.find() // TODO: query filter

        return res.json(items)
      } catch (e) {
        next(e)
      }
    })

    // create a project
    .post(async (req, res, next) => {
      try {
        const data = req.body
        await preSave(req, data)

        const item = new models.Project(data)
        await item.save()

        return res.status(201).json(item)
      } catch (e) {
        next(e)
      }
    })

  app.route('/projects/:project')
    // middleware for all methods
    .all(
      authentication.bearer,
      authorize('project')
    )

    // retrieve a project
    .get(async (req, res, next) => {
      try {
        const item = req.item

        res.json(item)
      } catch (e) {
        next(e)
      }
    })

    // update a project
    .patch(async (req, res, next) => {
      try {
        const data = req.body
        await preSave(req, data)

        const item = req.item
        await item.update(data)

        res.json(item)
      } catch (e) {
        next(e)
      }
    })

    // delete a project
    .delete(async (req, res, next) => {
      try {
        const item = req.item

        await item.remove()

        res.sendStatus(204)
      } catch (e) {
        next(e)
      }
    })
}
