const models = require('./models')
const routes = require('./routes')

module.exports = app => {
  models(app)
  routes(app)
}
