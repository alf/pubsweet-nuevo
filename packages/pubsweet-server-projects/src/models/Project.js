module.exports = mongoose => {
  const schema = mongoose.Schema({
    title: {
      type: String,
      required: true
    }
  }, {
    timestamps: true
  })

  return mongoose.model('Project', schema)
}
