const Project = require('./Project')

module.exports = app => {
  app.locals.models.Project = Project(app.locals.mongoose)
}
