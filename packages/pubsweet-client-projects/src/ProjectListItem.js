import React from 'react'
import { ListGroupItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const ProjectListItem = ({ project }) => (
  <LinkContainer to={`/projects/${project._id}`}>
    <ListGroupItem header={project.name}>
      {project.email}
    </ListGroupItem>
  </LinkContainer>
)

export default ProjectListItem
