import React from 'react'
import { refetch } from '@aeaton/pubsweet-client'
import { ListGroup } from 'react-bootstrap'
import ProjectListItem from './ProjectListItem'

const defaultListItem = project => (
  <ProjectListItem key={project._id} project={project}/>
)

const ProjectList = ({ fetch, listItem }) => {
  if (!fetch.fulfilled) return null

  return (
    <ListGroup>
      {fetch.value.map(listItem || defaultListItem)}
    </ListGroup>
  )
}

export default refetch(
  () => ({
    fetch: '/projects'
  })
)(ProjectList)
