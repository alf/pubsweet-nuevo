import React from 'react'
import { refetch } from '@aeaton/pubsweet-client'

const Project = ({ fetch, match }) => {
  if (!fetch.fulfilled) return null

  const project = fetch.value

  return (
    <div>
      <h1>{project.name}</h1>
    </div>
  )
}

export default refetch(
  ({ match }) => ({
    fetch: `/projects/${match.params.project}`
  })
)(Project)
