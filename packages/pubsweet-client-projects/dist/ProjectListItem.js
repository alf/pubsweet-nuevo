'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRouterBootstrap = require('react-router-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ProjectListItem = function ProjectListItem(_ref) {
  var project = _ref.project;
  return _react2.default.createElement(
    _reactRouterBootstrap.LinkContainer,
    { to: '/projects/' + project._id },
    _react2.default.createElement(
      _reactBootstrap.ListGroupItem,
      { header: project.name },
      project.email
    )
  );
};

exports.default = ProjectListItem;