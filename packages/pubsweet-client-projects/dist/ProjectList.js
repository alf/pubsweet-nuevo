'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _pubsweetClient = require('@aeaton/pubsweet-client');

var _reactBootstrap = require('react-bootstrap');

var _ProjectListItem = require('./ProjectListItem');

var _ProjectListItem2 = _interopRequireDefault(_ProjectListItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var defaultListItem = function defaultListItem(project) {
  return _react2.default.createElement(_ProjectListItem2.default, { key: project._id, project: project });
};

var ProjectList = function ProjectList(_ref) {
  var fetch = _ref.fetch,
      listItem = _ref.listItem;

  if (!fetch.fulfilled) return null;

  return _react2.default.createElement(
    _reactBootstrap.ListGroup,
    null,
    fetch.value.map(listItem || defaultListItem)
  );
};

exports.default = (0, _pubsweetClient.refetch)(function () {
  return {
    fetch: '/projects'
  };
})(ProjectList);