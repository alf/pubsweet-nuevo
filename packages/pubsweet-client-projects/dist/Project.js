'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _pubsweetClient = require('@aeaton/pubsweet-client');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var Project = function Project(_ref) {
  var fetch = _ref.fetch,
      match = _ref.match;

  if (!fetch.fulfilled) return null;

  var project = fetch.value;

  return _react2.default.createElement(
    'div',
    null,
    _react2.default.createElement(
      'h1',
      null,
      project.name
    )
  );
};

exports.default = (0, _pubsweetClient.refetch)(function (_ref2) {
  var match = _ref2.match;
  return {
    fetch: '/projects/' + match.params.project
  };
})(Project);