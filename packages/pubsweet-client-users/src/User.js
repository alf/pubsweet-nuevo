import React from 'react'
import { refetch } from '@aeaton/pubsweet-client'

const User = ({ fetch, match }) => {
  if (!fetch.fulfilled) return null

  const user = fetch.value

  return (
    <div>
      <h1>{user.name}</h1>
    </div>
  )
}

export default refetch(
  ({ match }) => ({
    fetch: `/users/${match.params.user}`
  })
)(User)
