import React from 'react'
import { ListGroupItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const UserListItem = ({ user }) => (
  <LinkContainer to={`/users/${user._id}`}>
    <ListGroupItem header={user.name}>
      {user.email}
    </ListGroupItem>
  </LinkContainer>
)

export default UserListItem
