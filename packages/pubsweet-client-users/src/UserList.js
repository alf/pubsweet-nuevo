import React from 'react'
import { refetch } from '@aeaton/pubsweet-client'
import { ListGroup } from 'react-bootstrap'
import UserListItem from './UserListItem'

const UserList = ({ fetch }) => {
  if (!fetch.fulfilled) return null

  const users = fetch.value

  return (
    <ListGroup>
      {users.map(user => (
        <UserListItem key={user._id} user={user}/>
      ))}
    </ListGroup>
  )
}

export default refetch(
  () => ({
    fetch: '/users'
  })
)(UserList)
