'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _pubsweetClient = require('@aeaton/pubsweet-client');

var _reactBootstrap = require('react-bootstrap');

var _UserListItem = require('./UserListItem');

var _UserListItem2 = _interopRequireDefault(_UserListItem);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserList = function UserList(_ref) {
  var fetch = _ref.fetch;

  if (!fetch.fulfilled) return null;

  var users = fetch.value;

  return _react2.default.createElement(
    _reactBootstrap.ListGroup,
    null,
    users.map(function (user) {
      return _react2.default.createElement(_UserListItem2.default, { key: user._id, user: user });
    })
  );
};

exports.default = (0, _pubsweetClient.refetch)(function () {
  return {
    fetch: '/users'
  };
})(UserList);