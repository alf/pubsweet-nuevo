'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactBootstrap = require('react-bootstrap');

var _reactRouterBootstrap = require('react-router-bootstrap');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UserListItem = function UserListItem(_ref) {
  var user = _ref.user;
  return _react2.default.createElement(
    _reactRouterBootstrap.LinkContainer,
    { to: '/users/' + user._id },
    _react2.default.createElement(
      _reactBootstrap.ListGroupItem,
      { header: user.name },
      user.email
    )
  );
};

exports.default = UserListItem;