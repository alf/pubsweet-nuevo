const bcrypt = require('bcrypt')

const BCRYPT_ROUNDS = Number(process.env.PUBSWEET_BCRYPT_ROUNDS) || 10

const compare = (password, existing) => {
  return bcrypt.compare(password, existing)
}

const hash = (password) => {
  return bcrypt.hash(password, BCRYPT_ROUNDS)
}

module.exports = { compare, hash }
