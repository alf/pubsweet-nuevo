const jwt = require('jsonwebtoken')

const sign = data => {
  return jwt.sign(data, process.env.PUBSWEET_SECRET, {
    expiresIn: 24 * 3600 // TODO: handle expiry in the client
  })
}

const verify = token => {
  return jwt.verify(token, process.env.PUBSWEET_SECRET)
}

module.exports = { sign, verify }
