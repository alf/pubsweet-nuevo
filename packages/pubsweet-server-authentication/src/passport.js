const passport = require('passport')
const LocalStrategy = require('passport-local')
const BearerStrategy = require('passport-http-bearer')
const encryption = require('./encryption')
const authToken = require('./token')

module.exports = app => {
  const models = app.locals.models

  app.use(passport.initialize())

  passport.use(new LocalStrategy((username, password, done) => {
    models.User.findOne({ username }).then(async user => {
      if (!user) {
        return done(new Error('User not found'))
      }

      try {
        await encryption.compare(password, user.password)
      } catch (e) {
        return done(new Error('Incorrect password'))
      }

      done(null, user)
    }).catch(done)
  }))

  passport.use(new BearerStrategy(async (token, done) => {
    try {
      const { id } = authToken.verify(token)

      const user = await models.User.findById(id)

      if (!user) {
        return done(new Error('User not found'))
      }

      done(null, user)
    } catch (e) {
      done(e)
    }
  }))

  app.locals.authentication = {
    local: passport.authenticate('local', { session: false }),
    bearer: passport.authenticate('bearer', { session: false })
  }

  module.exports = passport
}
