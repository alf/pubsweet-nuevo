const passport = require('./passport')
const encryption = require('./encryption')
const routes = require('./routes')

module.exports = app => {
  app.locals.encryption = encryption
  passport(app)
  routes(app)
}
