const encryption = require('./encryption')
const authToken = require('./token')

module.exports = app => {
  const models = app.locals.models

  app.post('/authentication/signup',
    // NOTE: no authentication
    async (req, res, next) => {
      try {
        const data = req.body
        delete data.admin
        data['password'] = await encryption.hash(data['password'])

        const user = new models.User(data)
        await user.save()

        const token = authToken.sign({ id: user.id })
        res.json({ user, token })
      } catch (e) {
        next(e)
      }
    }
  )

  app.post('/authentication/login',
    app.locals.authentication.local,
    (req, res, next) => {
      const user = req.user
      const token = authToken.sign({ id: user.id })
      res.json({ user, token })
    }
  )

  app.get('/authentication/user',
    app.locals.authentication.bearer,
    (req, res, next) => {
      const user = req.user
      res.json({ user })
    }
  )
}
